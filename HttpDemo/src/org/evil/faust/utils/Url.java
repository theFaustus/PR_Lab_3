package org.evil.faust.utils;

/**
 * Created by Faust on 3/19/2017.
 */
public class Url {
    private String url;
    private int index;

    public Url(String url, int index){
        this.url = url;
        this.index = index;
    }

    public int getIndex(){
        return index;
    }

    public String getUrl() {
        return url;
    }

}
