package org.evil.faust.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Created by Faust on 3/17/2017.
 */
public class Crawler {
    private int depth;
    private volatile boolean isStopped = false;
    private Set<String> visitedUrls;
    private final int STATUS_CODE_OK = 200;
    private ArrayList<Observer> observers;
    private Map<String, Set<String>> urlTree;
    private boolean isSearchMode = false;
    private String keyWord;
    private int numberOfOccurences = 0;
    private ExecutorService executor;

    public Crawler(int depth) {
        this.depth = depth;
        this.visitedUrls = Collections.synchronizedSet(new HashSet<String>());
        this.urlTree = new HashMap<>();
        this.observers = new ArrayList<>();
        this.executor = Executors.newFixedThreadPool(50, r -> {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setDaemon(true);
            return t;
        });
    }

    public Crawler(int depth, boolean mode, String keyWord) {
        this.depth = depth;
        this.isSearchMode = mode;
        this.keyWord = keyWord;
        this.observers = new ArrayList<>();
        this.visitedUrls = Collections.synchronizedSet(new HashSet<String>());
        this.urlTree = new HashMap<>();
        this.executor = Executors.newFixedThreadPool(50, r -> {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setDaemon(true);
            return t;
        });
    }

    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    public void unregisterObserver(Observer observer) {
        observers.remove(observer);
    }

    public boolean isRunning() {
        return isStopped;
    }

    public void publishUrl(String url) {
        for (Observer o : observers)
            o.consumeUrl(url);
    }

    public void publishNumberOfOccurences(Integer numberOfOccurences) {
        for (Observer o : observers)
            o.consumeNumberOfOccurences(numberOfOccurences);
    }

    public void crawl(String crawledUrl) {
        executor.submit(() -> {
            try {
                System.out.println("aaaaaaaaaaaaaaaaaaaaaa help");
                URL url = new URL(crawledUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(false);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setRequestMethod("GET");
                InputStream inputStream = httpURLConnection.getInputStream();


                try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                    String inputHtml = reader.lines().collect(Collectors.joining());

                    Pattern urlChecker = Pattern.compile("href=\"(.*?)\"");
                    Matcher matcher = urlChecker.matcher(inputHtml);
                    while (matcher.find() && !isStopped) {
                        String foundUrl = matcher.group(1);
                        if (!foundUrl.isEmpty()) {
                            foundUrl = makeUrlCanonical(foundUrl, url);
                            checkUrlAsync(foundUrl, crawledUrl);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    public String makeUrlCanonical(String url, URL baseUrl) {
        String result = "";
        if (url.startsWith("http"))
            return url;
        if (url.startsWith("//"))
            return baseUrl.getProtocol() + ":" + url;
        if (url.startsWith("/"))
            return baseUrl.getProtocol() + "://" + baseUrl.getHost() + url;
        if (!Character.isAlphabetic(url.charAt(0)))
            return "";
        if (url.startsWith("www"))
            return baseUrl.getProtocol() + "://" + url;
        if (Character.isAlphabetic(url.charAt(0)) && !url.startsWith("http"))
            return baseUrl.getProtocol() + "://" + baseUrl.getHost() + ":" + baseUrl.getPort() + "/" + url;


        return result;
    }


    public void checkUrlAsync(String childUrl, String parentUrl) {
        CompletableFuture<String> stringCompletableFuture = CompletableFuture.supplyAsync(() -> {
            if (isUrlOk(childUrl)) return childUrl;
            else return "";
        }, executor);
        stringCompletableFuture.thenAccept(s -> consumeValidUrl(s, parentUrl));
    }

    private synchronized void consumeValidUrl(String childUrl, String parentUrl) {

        boolean isAddSuccessful = visitedUrls.add(childUrl);
        if (!urlTree.containsKey(parentUrl))
            urlTree.put(parentUrl, new HashSet<>());
        Set<String> children = urlTree.get(parentUrl);
        children.add(childUrl);
        int distance = getDepthTillRoot(parentUrl);
        if (isAddSuccessful)
            if (!isSearchMode)
                publishUrl(childUrl);
            else
                searchKeywordInUrlAsync(childUrl, keyWord);
        if (distance < depth && isAddSuccessful){
            if (!childUrl.isEmpty()) {
                crawl(childUrl);
            }
        }


    }

    private int getDepthTillRoot(String parentUrl) {
        Set<String> visitedParents = new HashSet<>();
        int currentDepth = 0;
        String currentParent = parentUrl;
        while (currentParent != null) {
            ++currentDepth;
            if (visitedParents.add(currentParent)) {
                currentParent = getParent(currentParent);
            } else
                break;
        }
        return currentDepth;
    }

    private String getParent(String childUrl) {
        for (Map.Entry<String, Set<String>> entry : urlTree.entrySet()) {
            if (entry.getValue().contains(childUrl))
                return entry.getKey();
        }
        return null;
    }

    private void searchKeywordInUrlAsync(String childUrl, String keyWord) {
        CompletableFuture.supplyAsync(() -> searchKeyword(childUrl, keyWord), executor)
                .thenAccept(i -> consumeNumberOfOccurences(i));
    }

    private void consumeNumberOfOccurences(Integer numberOfOccurences) {
        publishNumberOfOccurences(numberOfOccurences);
    }

    private Integer searchKeyword(String url, String keyWord) {
        try {
            URL searchedUrl = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) searchedUrl.openConnection();
            httpURLConnection.setDoOutput(false);
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setRequestMethod("GET");
            InputStream inputStream = httpURLConnection.getInputStream();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == STATUS_CODE_OK)
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                    String inputHtml = reader.lines().collect(Collectors.joining());
                    Pattern searchKeyword = Pattern.compile(keyWord);
                    Matcher matcher = searchKeyword.matcher(inputHtml);
                    while (matcher.find() && !isStopped) {
                        numberOfOccurences++;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return numberOfOccurences;
    }

    public void stopCrawling() {
        isStopped = true;
        System.out.println("Done");
        executor.shutdownNow();
    }

    public boolean isUrlOk(String url) {
        try {
            URL verifiedUrl = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) verifiedUrl.openConnection();
            httpURLConnection.setDoOutput(false);
            httpURLConnection.setConnectTimeout(1000);
            httpURLConnection.setRequestMethod("HEAD");
            int responseCode = httpURLConnection.getResponseCode();
            httpURLConnection.getInputStream().close();
            if (responseCode == STATUS_CODE_OK)
                return true;
        } catch (Exception e) {
            return false;
        }
        return false;
    }


}
