package org.evil.faust.utils;

/**
 * Created by Faust on 3/19/2017.
 */
public interface Observer {
    void consumeUrl(String url);
    void consumeNumberOfOccurences(Integer numberOfOccurences);
}
