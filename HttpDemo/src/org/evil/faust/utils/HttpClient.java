package org.evil.faust.utils;

import javafx.util.Pair;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created by Faust on 3/18/2017.
 */
public class HttpClient implements Runnable {
    private final int STATUS_CODE_OK = 200;
    private final String GET_METHOD = "GET";
    private final String POST_METHOD = "POST";
    private final String HEAD_METHOD = "HEAD";


    private String url;
    private HttpURLConnection connection;
    private int responseCode;
    private String requestMethod;
    private InputStream inputStream;
    private String pageContent;
    private String headHeaders;
    private String postUrlParameters;
    private ArrayList<Pair<String, String>> requestMethodProperties;
    private Consumer<HttpClient> clientConsumer;
    private String host;
    private String protocol;


    public String getHeadHeaders() {
        return headHeaders;
    }

    public String getHost() {
        return host;
    }

    public String getProtocol() {
        return protocol;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public String getPageContent() {
        return pageContent;
    }

    public HttpClient(String url, ArrayList<Pair<String, String>> requestMethodProperties) {
        this.url = url;
        this.requestMethodProperties = requestMethodProperties;
    }

    public HttpClient(String url, String postUrlParameters, ArrayList<Pair<String, String>> requestMethodProperties) {
        this.url = url;
        this.postUrlParameters = postUrlParameters;
        this.requestMethodProperties = requestMethodProperties;
    }

    public void execute(String requestMethod) {
        this.pageContent = "nothing";
        this.requestMethod = requestMethod;
        new Thread(this).start();
    }

    private void openConnection() {
        URL url;
        try {
            url = new URL(this.url);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            this.host = url.getHost();
            this.protocol = url.getProtocol();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void executeGetMethodRequest() {
        try {
            connection.setRequestMethod("GET");
            if (!requestMethodProperties.isEmpty())
                for (Pair<String, String> e : requestMethodProperties) {
                    connection.setRequestProperty(e.getKey(), e.getValue());
                }
            connection.setDoOutput(false);
            inputStream = connection.getInputStream();
            responseCode = connection.getResponseCode();
            if (responseCode == STATUS_CODE_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                pageContent = reader.lines().collect(Collectors.joining());
                clientConsumer.accept(this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeHeadMethodRequest() {
        try {
            connection.setRequestMethod("HEAD");
            if (!requestMethodProperties.isEmpty())
                for (Pair<String, String> e : requestMethodProperties) {
                    connection.setRequestProperty(e.getKey(), e.getValue());
                }
            connection.setDoOutput(false);
            responseCode = connection.getResponseCode();
            if (responseCode == STATUS_CODE_OK) {
                Map<String, List<String>> map = connection.getHeaderFields();
                int i = 0;
                for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                    i++;
                    if (entry != null)
                        headHeaders += String.format("#%-7d%s - %s%n", i, entry.getValue().toString().startsWith("[HTTP") ? "Status Line" : entry.getKey(), entry.getValue());
                }
                headHeaders = headHeaders.replace("null", "");
                clientConsumer.accept(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executePostMethodRequest() {
        try {
            connection.setRequestMethod("POST");
            for (Pair<String, String> e : requestMethodProperties) {
                connection.setRequestProperty(e.getKey(), e.getValue());
            }
            connection.setDoOutput(true);
            if (!Objects.equals(postUrlParameters, "")) {
                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                dataOutputStream.write(URLEncoder.encode(postUrlParameters, "UTF-8").getBytes());
                dataOutputStream.flush();
                dataOutputStream.close();
            }
            inputStream = connection.getInputStream();
            responseCode = connection.getResponseCode();
            if (responseCode == STATUS_CODE_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                pageContent = reader.lines().collect(Collectors.joining());
                clientConsumer.accept(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeConnection() {
        if (inputStream != null)
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        if (connection != null)
            connection.disconnect();
    }

    public void onResult(Consumer<HttpClient> consumer) {
        clientConsumer = consumer;
    }

    @Override
    public void run() {
        switch (requestMethod) {
            case GET_METHOD:
                openConnection();
                executeGetMethodRequest();
                closeConnection();
                break;
            case HEAD_METHOD:
                openConnection();
                executeHeadMethodRequest();
                closeConnection();
                break;
            case POST_METHOD:
                openConnection();
                executePostMethodRequest();
                closeConnection();
                break;
            default:
                break;
        }
    }

    public static boolean isUrl(String url) {
        return url.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
    }
}
