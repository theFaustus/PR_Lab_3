package org.evil.faust.sample;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import javafx.util.Pair;
import org.evil.faust.utils.Crawler;
import org.evil.faust.utils.HttpClient;
import org.evil.faust.utils.Observer;
import org.evil.faust.utils.Url;

import java.io.Closeable;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class Controller implements Observer, Initializable {

    @FXML
    public TextField urlTextField;
    @FXML
    public WebView webView;
    @FXML
    public TextField hostText;
    @FXML
    public TextField protocolText;
    @FXML
    public TextField statusCodeText;
    @FXML
    public TextArea headText;
    @FXML
    public TextField property1;
    @FXML
    public TextField value1;
    @FXML
    public TextField property2;
    @FXML
    public TextField value2;
    @FXML
    public TextField property3;
    @FXML
    public TextField value3;
    @FXML
    public TextField property4;
    @FXML
    public TextField value4;
    @FXML
    public TextField property5;
    @FXML
    public TextField value5;
    @FXML
    public TextField property6;
    @FXML
    public TextField value6;
    @FXML
    public TextField formDataTextField;
    @FXML
    public TextField crawlDepthTextField;
    @FXML
    public TextField crawlUrlTextField;
    @FXML
    public TableColumn<Url, String> visitedUrlColumn;
    @FXML
    public TableView crawlerTableView;
    @FXML
    public TextField searchKeywordTextField;
    @FXML
    public TextField numberOfOccurencesTextField;
    @FXML
    public TableColumn<Url, String> numberOfVisitedUrl;

    private Crawler crawler;
    private Crawler searchCrawler;
    private int indexOfVisitedUrls;

    @FXML
    public void onGetButtonPressed(ActionEvent actionEvent) {
        String url = urlTextField.getText();
        if (HttpClient.isUrl(url)) {
            WebEngine webEngine = webView.getEngine();
            HttpClient httpClient = new HttpClient(url, getRequestMethodProperties());
            httpClient.execute("GET");
            httpClient.onResult(c -> Platform.runLater(() -> {
                webEngine.loadContent(c.getPageContent());
                initializeBasicHttpTextFields(c);
            }));
        } else {
            alertThatUrlIsBroken();
        }
    }

    private void initializeBasicHttpTextFields(HttpClient c) {
        protocolText.setText(c.getProtocol());
        statusCodeText.setText(c.getResponseCode() + "");
        hostText.setText(c.getHost());
    }

    @FXML
    public void onHeadButtonPressed(ActionEvent actionEvent) {
        String url = urlTextField.getText();
        if (HttpClient.isUrl(url)) {
            HttpClient httpClient = new HttpClient(url, getRequestMethodProperties());
            httpClient.execute("HEAD");
            httpClient.onResult(c -> Platform.runLater(() -> {
                initializeBasicHttpTextFields(c);
                headText.setText(c.getHeadHeaders());
            }));
        } else {
            alertThatUrlIsBroken();
        }
    }

    @FXML
    public void onPostButtonPressed(ActionEvent actionEvent) {
        String url = urlTextField.getText();
        if (HttpClient.isUrl(url)) {
            WebEngine webEngine = webView.getEngine();
            HttpClient httpClient = new HttpClient(url, formDataTextField.getText(), getRequestMethodProperties());
            httpClient.execute("POST");
            httpClient.onResult(c -> Platform.runLater(() -> {
                webEngine.loadContent(c.getPageContent());
                initializeBasicHttpTextFields(c);
            }));
        } else {
            alertThatUrlIsBroken();
        }
    }

    @FXML
    public void alertThatUrlIsBroken() {
        Alert alert = new Alert(Alert.AlertType.ERROR, "Oops, looks like you entered a wrong url.\nTry for example: https://www.google.com", ButtonType.CLOSE);
        alert.setHeaderText("Broken url");
        alert.setTitle("Error Dialog");
        alert.showAndWait();
    }

    @FXML
    public ArrayList<Pair<String, String>> getRequestMethodProperties() {
        ArrayList<Pair<String, String>> requestMethodProperties = new ArrayList<>();
        if (!Objects.equals(property1.getText(), "") && !Objects.equals(value1.getText(), ""))
            requestMethodProperties.add(new Pair<>(property1.getText(), value1.getText()));
        if (!Objects.equals(property2.getText(), "") && !Objects.equals(value2.getText(), ""))
            requestMethodProperties.add(new Pair<>(property2.getText(), value2.getText()));
        if (!Objects.equals(property3.getText(), "") && !Objects.equals(value3.getText(), ""))
            requestMethodProperties.add(new Pair<>(property3.getText(), value3.getText()));
        if (!Objects.equals(property4.getText(), "") && !Objects.equals(value4.getText(), ""))
            requestMethodProperties.add(new Pair<>(property4.getText(), value4.getText()));
        if (!Objects.equals(property5.getText(), "") && !Objects.equals(value5.getText(), ""))
            requestMethodProperties.add(new Pair<>(property5.getText(), value5.getText()));
        if (!Objects.equals(property6.getText(), "") && !Objects.equals(value6.getText(), ""))
            requestMethodProperties.add(new Pair<>(property6.getText(), value6.getText()));
        return requestMethodProperties;
    }

    @FXML
    public void onCrawlButtonPressed(ActionEvent actionEvent) {
        indexOfVisitedUrls = 0;
        crawlerTableView.getItems().clear();
        String url = crawlUrlTextField.getText();
        int depth = Integer.parseInt(crawlDepthTextField.getText());
        if (HttpClient.isUrl(url)) {
            if (crawler != null)
                crawler.stopCrawling();
            crawler = new Crawler(depth);
            crawler.registerObserver(this);
            crawler.crawl(url);
        } else {
            alertThatUrlIsBroken();
        }
    }

    @FXML
    public void onSearchButtonPressed(ActionEvent actionEvent) {
        String url = crawlUrlTextField.getText();
        int depth = Integer.parseInt(crawlDepthTextField.getText());
        if (HttpClient.isUrl(url)) {
            if (searchCrawler != null)
                searchCrawler.stopCrawling();
            searchCrawler = new Crawler(depth, true, searchKeywordTextField.getText());
            searchCrawler.registerObserver(this);
            searchCrawler.crawl(url);
        } else {
            alertThatUrlIsBroken();
        }
    }

    @FXML
    public void onStopCrawlButtonPressed(ActionEvent actionEvent) {
        if (!crawler.isRunning()) {
            crawler.stopCrawling();
        }
    }

    @FXML
    public void onStopSearchButtonPressed(ActionEvent actionEvent) {
        if (!searchCrawler.isRunning()) {
            searchCrawler.stopCrawling();
        }
    }

    @Override
    public void consumeUrl(String url) {
        if (!url.isEmpty()) {
            synchronized (this){
                indexOfVisitedUrls++;
            }
            Url displayedUrl = new Url(url, indexOfVisitedUrls);
            Platform.runLater(() -> {
                crawlerTableView.getItems().add(displayedUrl);
                crawlerTableView.scrollTo(displayedUrl);
            });

        }


    }

    @Override
    public void consumeNumberOfOccurences(Integer numberOfOccurences) {
        Platform.runLater(() -> {
            numberOfOccurencesTextField.setText(numberOfOccurences + "");

        });
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        visitedUrlColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getUrl()));
        numberOfVisitedUrl.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getIndex() + ""));
    }


}

